﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HiandLo
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        int kazu = 0;

        public Form1()
        {
            InitializeComponent();
        }
        //Highボタンが押されたときのイベント、ちなみにねらーじゃない
        private void button1_Click(object sender, EventArgs e)
        {
            if (kazu >= 100)
            {
                label2.Text = "ｱﾀﾘ！ヽ(●´∀`)人(´∀`●)ﾉ";
            }
            else
            {
                label2.Text = "ﾊｽﾞﾚ!ｳｿ━Σ(-`Д´-;)━ﾝ!!";
            }
            label1.Text = kazu.ToString();
            button1.Enabled = false;
            button2.Enabled = false;
        }

        //Lowボタンが押されたときのイベントじゃ
        private void button2_Click(object sender, EventArgs e)
        {
            if (kazu <= 99)
            {
                label2.Text = "ｱﾀﾘｷﾀ━━━(ﾟ∀ﾟ)━━━!!!";
            }
            else
            {
                label2.Text = "ﾊｽﾞﾚ！( ´∀`)ｹﾞﾗｹﾞﾗ";
            }
            label1.Text = kazu.ToString();
            button1.Enabled = false;
            button2.Enabled = false;
        }

        //そのまんまResetボ…ボタンじゃい！
        private void resetButton(object sender, EventArgs e)
        {
            init();
            
        }

        private void init()
        {
            label1.Text = "( ? )";
            label2.Text = "？は100より大きいか小さいか！"; //食い込んでるか食い込んでないか？
            kazu = rand.Next(1, 200); //1-200までの乱数
            Console.WriteLine("Random No." + kazu);
            button1.Enabled = true;
            button2.Enabled = true;
        }

        //起動初期処理
        private void Form1_Load(object sender, EventArgs e)
        {
            init();
          
        }
    }
}
